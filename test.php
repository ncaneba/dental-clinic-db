		<?php
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$patientid = isset($_GET['patientid']) ? $_GET['patientid'] : '';
			$chart = isset($_GET['chartid']) ? $_GET['chartid'] : '';

			$sql = "SELECT *
					FROM patient
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);
  
			oci_fetch($stid);
			
			$pfirstname = oci_result($stid, 'PFIRST_NAME');
			$plastname = oci_result($stid, 'PLAST_NAME');
			
			$sql = "SELECT *
					FROM dental_chart
					WHERE chart_id = :ch";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':ch', $chart);

			oci_execute($stid);
			
			oci_fetch($stid);
			
			$chart = oci_result($stid, 'CHART_ID')

			echo '<form class="container" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post" id="needs-validation" novalidate>
<div>
<label>.'$pfirstname'.</label>
<label>.'$plastname'.</label>
<label>.'$chart'.</label>
</div>
<div class="col-md-1 mb-1">
	  <label> Record No. : </label>
      <input type="text" name="records" class="form-control" id="validationCustom01" style="text-transform: Capitalize" placeholder="" required>
	  <div class="invalid-feedback">
      </div>
    </div>

	<br><br>

<div class="row">
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes1" class="form-control" id="validationCustom05" value="1" maxlength="2">
      <input type="text" name="status1" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
</div>
<br><br>
  <div class="btn-group" role="group">
  <button class="btn btn-primary mb-2" type="submit" name="add"><i class ="fa fa-plus" aria-hidden="true"></i></button>
  <a class="btn btn-default mb-2" type="submit"  href="table_treatment.php" ><i class ="fa fa-arrow-left" aria-hidden="true"></i></a>
  </div>
</form>';

			oci_close($conn);
		?>