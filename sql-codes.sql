/*
SII Dental Clinic SQL Codes

Revision :
March 10,2018
- Remove Birthdate in PATIENT table
- Add precision and scale in AMOUNT_PAID in PAYMENT table
- Add Data for TEETH table

Created By: Chrstian Kim Escamilla
Copyright © SII Dental Clinic 2017
*/




CREATE TABLE PATIENT
(PATIENT_ID NUMBER(20)  PRIMARY KEY NOT NULL,
PFIRST_NAME VARCHAR (30) NOT NULL,
PMIDDLE_INITIAL  VARCHAR (2) NULL,
PLAST_NAME VARCHAR (20) NOT NULL,
ADDRESS VARCHAR (300) NOT NULL,
AGE NUMBER NOT NULL,
Contact_Num NUMBER(11)  NOT NULL);

CREATE TABLE PAYMENT
(TYPE_OF_PAYMENT	VARCHAR (20)		NOT NULL,
AMOUNT_PAID		DECIMAL(7,2)		NOT NULL,
DATE_PAID		DATE			NOT NULL,
PATIENT_ID NUMBER (20) not null, FOREIGN KEY(PATIENT_ID) REFERENCES 
PATIENT(PATIENT_ID));

CREATE TABLE DENTIST
(DOCTOR_ID		NUMBER PRIMARY KEY		NOT NULL,
FNAME			VARCHAR (30)		NOT NULL,
MINITIAL			VARCHAR (2)		NOT NULL,
LNAME			VARCHAR (20)		NOT NULL,
CONTACT_NUMBER	NUMBER(12)		NOT NULL);


CREATE TABLE CHECK_UP
(CHECK_UP_ID NUMBER(20) PRIMARY KEY NOT NULL,
CHECK_UP_DATE DATE NOT NULL,
CHECK_UP_TIME DATE NOT NULL,
CHECK_UP_DETAILS VARCHAR(300) NOT NULL,
PATIENT_ID NUMBER(20) not null, FOREIGN KEY(PATIENT_ID) REFERENCES 
PATIENT(PATIENT_ID),
DOCTOR_ID NUMBER(20) not null, FOREIGN KEY(DOCTOR_ID) REFERENCES DENTIST(DOCTOR_ID));

CREATE TABLE DENTAL_CHART
(CHART_ID		NUMBER(20) PRIMARY KEY NOT NULL,
DATES			DATE	NOT NULL,
PATIENT_ID NUMBER (20) NOT NULL, FOREIGN KEY(PATIENT_ID) REFERENCES 
PATIENT(PATIENT_ID));

CREATE TABLE TEETH
(TEETH_CODE		NUMBER(20)	PRIMARY KEY NOT NULL,
TEETH_NAME		VARCHAR (30)		NOT NULL
);

CREATE TABLE TREATMENT
(TREATMENT_ID NUMBER PRIMARY KEY NOT NULL,
TREATMENT_TYPE VARCHAR (300) NOT NULL,
PRICE NUMBER(10,2) NOT NULL);


CREATE TABLE TREATMENT_LOG
(TREATMENT_LOG_ID NUMBER(20) PRIMARY KEY NOT NULL,
REMARKS VARCHAR(30) NOT NULL,
 TREATMENT_ID NUMBER(20) NOT NULL, FOREIGN KEY(TREATMENT_ID) REFERENCES TREATMENT(TREATMENT_ID),
CHECK_UP_ID NUMBER(20) NOT NULL,FOREIGN KEY(CHECK_UP_ID) REFERENCES CHECK_UP(CHECK_UP_ID));

CREATE TABLE TEETH_RECORD
(RECORD_NO NUMBER(20) NOT NULL,
STATUS VARCHAR(20) NOT NULL,
TEETH_CODE NUMBER(20) NOT NULL,FOREIGN KEY(TEETH_CODE) REFERENCES TEETH(TEETH_CODE),
CHART_ID NUMBER(20) NOT NULL,FOREIGN KEY(CHART_ID) REFERENCES DENTAL_CHART(CHART_ID),
PATIENT_ID NUMBER(20) NOT NULL,FOREIGN KEY(PATIENT_ID) REFERENCES PATIENT(PATIENT_ID));

CREATE TABLE TREATMENT_SERVICE
(TREATMENT_SERVICE_ID	NUMBER PRIMARY KEY NOT NULL,
TEETH_CODE NUMBER(20) NOT NULL,FOREIGN KEY(TEETH_CODE) REFERENCES TEETH(TEETH_CODE),
TREATMENT_LOG_ID NUMBER(20) NOT NULL,FOREIGN KEY(TREATMENT_LOG_ID) REFERENCES TREATMENT_LOG(TREATMENT_LOG_ID));


CREATE SEQUENCE seq_patient
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE SEQUENCE seq_dentist
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE SEQUENCE seq_treatment
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE SEQUENCE seq_check
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE SEQUENCE seq_payment
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE SEQUENCE seq_teeth
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE SEQUENCE seq_chart
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE SEQUENCE seq_records
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Wisdom Tooth (3rd Molar)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Molar (2nd Molar)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Molar (1st Molar)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Bicuspid (2nd)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Bicuspid (1st)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Canine (Eye tooth/Cuspid))');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Incisor (Lateral)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Incisor (Central)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Incisor (Central)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Incisor (Lateral)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Canine (Eye tooth/Cuspid))');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Bicuspid (1st)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Bicuspid (2nd)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Molar (1st Molar)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Molar (2nd Molar)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Wisdom Tooth (3rd Molar)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Wisdom Tooth (3rd Molar)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Molar (2nd Molar)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Molar (1st Molar)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Bicuspid (2nd)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Bicuspid (1st)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Canine (Eye tooth/Cuspid))');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Incisor (Lateral)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Incisor (Central)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Incisor (Central)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Incisor (Lateral)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Canine (Eye tooth/Cuspid))');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Bicuspid (1st)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Bicuspid (2nd)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Molar (1st Molar)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Molar (2nd Molar)');

INSERT INTO teeth (teeth_code, teeth_name)
VALUES (seq_teeth.NEXTVAL,'Wisdom Tooth (3rd Molar)');

commit;