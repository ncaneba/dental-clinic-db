<style>
 #errors {
   color: #FF0000;
}
</style>

<?php

    // Start the session
    session_start();

    // Defines username and password. Retrieve however you like,
    $username = "dental";
    $password = "dental";

    // Error message
    $error = "";

    // Checks to see if the user is already logged in. If so, refirect to correct page.
    if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == true) {
        $error = "success";
        header('Location: index.php');
    } 
        
    // Checks to see if the username and password have been entered.
    // If so and are equal to the username and password defined above, log them in.
    if (isset($_POST['username']) && isset($_POST['password'])) {
        if ($_POST['username'] == $username && $_POST['password'] == $password) {
            $_SESSION['loggedIn'] = true;
            header('Location: index.php');
        } else {
            $_SESSION['loggedIn'] = false;
            $error = '<p style="color:#FF0000">*Invalid Username or Password</p>';
        }
    }
?>
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>SII Dental Clinic</title>
    <meta name="description" content="Flat UI Kit Free is a Twitter Bootstrap Framework design and Theme, this responsive framework includes a PSD and HTML version."/>
    <meta name="viewport" content="width=1000, initial-scale=1.0, maximum-scale=1.0">  
    <!-- Loading Bootstrap -->
    <link href="dist/css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Loading Flat UI -->
 <!--   <link href="dist/css/flat-ui.css" rel="stylesheet">
    <link href="docs/assets/css/demo.css" rel="stylesheet"> -->

    <link rel="shortcut icon" href="img/teeth.ico">
      <link rel="stylesheet" href="css/style.css">

  
</head>

<body>
  <body>
  
<div class="app-head">
	<p><img src="images/logo-icon.png"></p>
	<h1><p class="text-white"> SII Dental Clinic</p></h1>
</div>
	<div class="login">
		<div class="login-screen">
			<div class="app-title">
				<h1>Welcome!</h1>
			</div>

			<div class="login-form">
				<form method="post" action="login.php">
				
				<strong><font color="red"><?php echo $error; ?></font></strong>
				
				<div class="control-group">
				<input type="text" class="login-field" value="" placeholder="username" id="login-name" name="username">
				<label class="login-field-icon fui-user" for="login-name"></label>
				</div>

				<div class="control-group">
				<input type="password" class="login-field" value="" placeholder="password" id="login-pass" name="password">
				<label class="login-field-icon fui-lock" for="login-pass"></label>
				</div>

				<button type="submit" class="btn" name="login"> Login </button>
				<a class="login-link" href="#">Lost your password?</a>
				
				</form>
			</div>
		</div>
	</div>
</body>
  
  
</body>
</html>
