<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

		<?php
		if(isset($_GET['chartid'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$chartid = $_GET['chartid'];

			$sql = "DELETE
					FROM dental_chart
					WHERE chart_id = :ci";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':ci', $chartid);

			oci_execute($stid);
			    header("Location: table_chart.php");
			oci_close($conn);
		}
		?>
				