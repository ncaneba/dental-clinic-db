<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Manage Payments</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
	      <!-- Search Tab-->

		  <br>	
		
		
		<?php
		if(isset($_GET['patientid'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$patientid = $_GET['patientid'];

			$sql = "SELECT *
					FROM payment
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);

			if (empty($stid)) {
				
				echo ' No Data Found';
				
			} else {
			
echo '<table class="table table-bordered" cellspacing="0">
<thread>
				  <tr class="active">
					  <th>Payment ID</th>
					  <th>Type of Payment</th>
					  <th>Amount Paid</th>
					  <th>Date Paid</th>
					  <th>Patient ID</th>
					  <th>Actions</th>
				  </tr></thread>';
while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
    echo "<tr>\n";
    foreach ($row as $item) {
        echo "    <td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
    }
				echo '<td><div class="btn-group" role="group"><a href="edit_payment.php?paymentid='.htmlentities($row['PAYMENT_ID']).'" class="btn btn-primary"><i class ="fa fa-pencil" aria-hidden="true"></i></a>
				  <a class="btn btn-danger" href="delete_payment.php?paymentid='.htmlentities($row['PAYMENT_ID']).'" onclick=\'return confirm("Are you sure you want to delete this record?");\'><i class ="fa fa-trash-o" aria-hidden="true"></i></div></td>';
    echo "</tr>\n";
}
echo "</table>\n";
			}
			oci_close($conn);
		}
		?>

				</div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>