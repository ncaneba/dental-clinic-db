<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Manage Charts</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
	      <!-- Start Add Chart-->  
	<?php
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$patientid = isset($_GET['patientid']) ? $_GET['patientid'] : '';
			$chart = isset($_GET['chartid']) ? $_GET['chartid'] : '';

			$sql = "SELECT *
					FROM patient
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);
  
			oci_fetch($stid);
			
			$pfirstname = oci_result($stid, 'PFIRST_NAME');
			$plastname = oci_result($stid, 'PLAST_NAME');
			
			echo'
			<h4>
			<label>Name :'.$plastname.',</label>
			<label>'.$pfirstname.'</label>
			</h4>
			';
			
			?>

		  <?php
		if(isset($_GET['patientid'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$patientid = $_GET['patientid'];

			$sql = "SELECT *
					FROM dental_chart
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);
			
			if ($line = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
    	
				echo 'Add Chart : <a href="insert_chart.php?patientid='.htmlentities($line['PATIENT_ID']).'" class="btn btn-primary"><i class ="fa fa-plus" aria-hidden="true"></i></a>';
		}	
				
			oci_close($conn);
		}
		?>
		
		<!-- End Add Chart-->
		  
<!--		  Add Chart:
			<li class = "btn btn-space"><a href="insert_chart.php" class="btn btn-primary" ><i class ="fa fa-plus" aria-hidden="true"></i></a></li>
-->
			<br>	
		
		  <br>	
		
		
		<?php
		if(isset($_GET['patientid'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$patientid = $_GET['patientid'];

			$sql = "SELECT *
					FROM dental_chart
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);

echo '<table class="table table-bordered" cellspacing="0">
<thread>
				  <tr class="active">
					  <th>Chart ID</th>
					  <th>Date Added (DD-MM-YY)</th>
					  <th>Patient ID</th>
					  <th>Actions</th>
				  </tr></thread>';
while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
    echo "<tr>\n";
    foreach ($row as $item) {
        echo "    <td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
    }
				echo '<td>
				
				<a href="table_records.php?patientid='.htmlentities($row['PATIENT_ID']).'&&chartid='.htmlentities($row['CHART_ID']).'" class="btn btn-primary"><i class ="fa fa-eye" aria-hidden="true"></i></a>
				  <a class="btn btn-danger" href="delete_chart.php?chartid='.htmlentities($row['CHART_ID']).'" onclick=\'return confirm("Are you sure you want to delete this record?");\'><i class ="fa fa-trash-o" aria-hidden="true"></i></a></td>';
    echo "</tr>\n";
}
echo "</table>\n";
			
			oci_close($conn);
		}
		?>
		<label>Back: </label>
<li class = "btn btn-space"><a href="action_chart.php" class="btn btn-primary" ><i class ="fa fa-arrow-left" aria-hidden="true"></i></a></li>
				</div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>