<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Appointments</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
	      <!-- Search Tab--
		 <form class="form-inline my-2 my-lg-0 mr-lg-2" action="search_patient.php?search=" method="get">
			<div class="input-group">
              <input class="form-control" name="search" placeholder="Search..." style="text-transform: capitalize" type="text">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-primary">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
			<li class = "btn btn-space"><a href="insert_patient.php" class="btn btn-primary" ><i class ="fa fa-plus" aria-hidden="true"></i></a></li>
			<li class = "btn btn-space"><a href="table_patient.php"><i  class="fa fa-refresh" aria-hidden="true"></i></a></li>
          </form>-->
<h4>Add Check-up : <a href="action_checkup.php" class="btn btn-primary"><i class ="fa fa-plus" aria-hidden="true"></i></a></h4>
		  <br>
		
<?php

// Connects to the XE service (i.e. database) on the "localhost" machine
$conn = oci_connect('dental', 'dental', 'localhost/XE');
if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

$stid = oci_parse($conn, 'SELECT check_up_id, check_up_date, check_up_time, check_up_details, patient_id FROM check_up ORDER BY check_up_id ASC');
oci_execute($stid);

echo '
<div class="table-responsive">
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
<thread>
				  <tr class="active">
					  <th>Check-up ID</th>
					  <th>Check-up Date</th>
					  <th>Check-up Time</th>
					  <th>Check-up Details</th>
					  <th>Patient ID</th>
					  <th>Actions</th>
				  </tr></thread>';
while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
    echo "<tr>\n";
    foreach ($row as $item) {
        echo "    <td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
    }
				echo '<td><div class="btn-group" role="group">
				<a href="view_patient.php?patientid='.htmlentities($row['PATIENT_ID']).'" class="btn btn-primary"><i class ="fa fa-eye" aria-hidden="true"></i></a>
				<a href="edit_checkup.php?checkupid='.htmlentities($row['CHECK_UP_ID']).'" class="btn btn-primary"><i class ="fa fa-pencil" aria-hidden="true"></i></a>
				  <a class="btn btn-danger" href="delete_checkup.php?checkupid='.htmlentities($row['CHECK_UP_ID']).'" onclick=\'return confirm("Are you sure you want to delete this record?");\'><i class ="fa fa-trash-o" aria-hidden="true"></i></div></td>';
   echo "</tr>\n";
}
echo "</table>\n";
echo "</div>\n";

?>

				</div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>