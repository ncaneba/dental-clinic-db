<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Edit Patient</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
		<!--update operation-->
		<?php
		if(isset($_POST['edit'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}
  
			$patientid = isset($_GET['patientid']) ? $_GET['patientid'] : '';
  
			$sql = "SELECT *
					FROM patient
					WHERE patient_id = :pi";
  
			$stid = oci_parse($conn, $sql);
  
			oci_bind_by_name($stid, ':pi', $patientid);
  
			oci_execute($stid);
  
			oci_fetch($stid);
  
			$oldfirstname = oci_result($stid, 'PFIRST_NAME');
			$oldmiddleinitial = oci_result($stid, 'PMIDDLE_INITIAL');
			$oldlastname = oci_result($stid, 'PLAST_NAME');
			$oldaddress = oci_result($stid, 'ADDRESS');
			$oldage = oci_result($stid, 'AGE');
			$oldcontactno = oci_result($stid, 'CONTACT_NUM');
  
			$pfirstname = !empty($_POST['pfirstname']) ? $_POST['pfirstname'] : $oldfirstname;
			$pmiddleinitial = !empty($_POST['pmiddleinitial']) ? $_POST['pmiddleinitial'] : $oldmiddleinitial;
			$plastname = !empty($_POST['plastname']) ? $_POST['plastname'] : $oldlastname;
			$address = !empty($_POST['address']) ? $_POST['address'] : $oldaddress;
			$age = !empty($_POST['age']) ? $_POST['age'] : $oldage;
			$pcontactno = !empty($_POST['pcontactno']) ? $_POST['pcontactno'] : $oldcontactno;
			
			$sql = "UPDATE patient
					SET pfirst_name = :fn, pmiddle_initial = :mi, plast_name = :ln, address = :ad, age = :ag, contact_num = :cn
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':fn', $pfirstname);
			oci_bind_by_name($stid, ':mi', $pmiddleinitial);
			oci_bind_by_name($stid, ':ln', $plastname);
			oci_bind_by_name($stid, ':ad', $address);
			oci_bind_by_name($stid, ':ag', $age);

			oci_bind_by_name($stid, ':cn', $pcontactno);

			oci_execute($stid);
		    header("Location: table_patient.php");

			oci_close($conn);
		}
		?>
		<!--end update operation-->
		<!--update form-->
		<?php
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$patientid = $_GET['patientid'];

			$sql = "SELECT *
					FROM patient
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);
  
			oci_fetch($stid);
			
			$pfirstname = oci_result($stid, 'PFIRST_NAME');
			$pmiddleinitial = oci_result($stid, 'PMIDDLE_INITIAL');
			$plastname = oci_result($stid, 'PLAST_NAME');
			$address = oci_result($stid, 'ADDRESS');
			$age = oci_result($stid, 'AGE');

			$pcontactno = oci_result($stid, 'CONTACT_NUM');
  
			echo '<div class="container">
<form class="container" action="" method="post" id="needs-validation" novalidate>
 <div class="row">
    <div class="col-md-5 mb-3">
      <label for="validationCustom01">First name</label>
      <input type="text" name="pfirstname" class="form-control" value="'.$pfirstname.'" id="validationCustom01" style="text-transform: Capitalize" placeholder="First name" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-2 mb-3">
      <label for="validationCustom01">Middle Initial</label>
      <input type="text" name="pmiddleinitial" class="form-control" value="'.$pmiddleinitial.'" id="validationCustom01" style="text-transform: Capitalize" placeholder="MI" maxlength="2" >
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-5 mb-3">
      <label for="validationCustom02">Last name</label>
      <input type="text" name="plastname" class="form-control" value="'.$plastname.'" id="validationCustom02" style="text-transform: Capitalize" placeholder="Last name" required>
	  <div class="invalid-feedback">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 mb-3">
      <label for="validationCustom03">Address</label>
      <input type="text" name="address" class="form-control" value="'.$address.'" id="validationCustom03" style="text-transform: Capitalize" placeholder="123 Sample St., Sample City" required>
      <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-2 mb-3">
      <label for="validationCustom03">Age</label>
      <input type="text" name="age" class="form-control" value="'.$age.'" id="validationCustom05" maxlength="3" onkeypress="return isNumber(event)" placeholder="Age" required>
      <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-3 mb-3">
      <label for="validationCustom05">Contact No.</label>
      <input type="text" name="pcontactno" class="form-control" value="'.$pcontactno.'" id="validationCustom05" maxlength="11" onkeypress="return isNumber(event)" placeholder="09XXXXXXXXX" required>
      <div class="invalid-feedback">
      </div>
    </div>
  </div>
  </div>
    <!--<input type="submit" class="btn btn-primary" name="add" value="Add patient"> -->
	<br>

    <!--<input type="submit" class="btn btn-primary" name="add" value="Add patient"> -->
  <div class="bottom-left">
  <div class="btn-group" role="group">
  <button class="btn btn-primary mb-2" type="submit" name="edit"><i class ="fa fa-plus" aria-hidden="true"></i></button>
  <a class="btn btn-default mb-2" type="submit"  href="table_patient.php" ><i class ="fa fa-arrow-left" aria-hidden="true"></i></a>
  </div>
  </div>
</form>
				  </div>';

			oci_close($conn);
		?>
		<!--end update form-->

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('needs-validation');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
})();
</script>

<script type="text/javascript"> 
// Only Number Input    
function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if ( (charCode > 31 && charCode < 48) || charCode > 57) {
            return false;
        }
        return true;
    }
</script>
	
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>