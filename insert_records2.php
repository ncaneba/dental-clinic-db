<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>
<link href="css/example.css" rel="stylesheet">
<!-- <link href="css/custom.sass" rel="stylesheet"> -->

<style>

	ZAS
</style>

<?php include('header.php') ?>


  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Add Teeth Code</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
		<!--add operation-->
		<?php
		if(isset($_POST['add'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$patientid = $_GET['patientid'];
			$chart = $_GET['chartid'];
			$teeth_records = isset($_POST['records']) ? $_POST['records'] : '';
			$status1 = isset($_POST['status1']) ? $_POST['status1'] : '';
			$status2 = isset($_POST['status2']) ? $_POST['status2'] : '';
			$status3 = isset($_POST['status3']) ? $_POST['status3'] : '';
			$status4 = isset($_POST['status4']) ? $_POST['status4'] : '';
			$status5 = isset($_POST['status5']) ? $_POST['status5'] : '';
			$status6 = isset($_POST['status6']) ? $_POST['status6'] : '';
			$status7 = isset($_POST['status7']) ? $_POST['status7'] : '';
			$status8 = isset($_POST['status8']) ? $_POST['status8'] : '';
			$status9 = isset($_POST['status9']) ? $_POST['status9'] : '';
			$status10 = isset($_POST['status10']) ? $_POST['status10'] : '';
			$status11 = isset($_POST['status11']) ? $_POST['status11'] : '';
			$status12 = isset($_POST['status12']) ? $_POST['status12'] : '';
			$status13 = isset($_POST['status13']) ? $_POST['status13'] : '';
			$status14 = isset($_POST['status14']) ? $_POST['status14'] : '';
			$status15 = isset($_POST['status15']) ? $_POST['status15'] : '';
			$status16 = isset($_POST['status16']) ? $_POST['status16'] : '';
			$status17 = isset($_POST['status17']) ? $_POST['status17'] : '';
			$status18 = isset($_POST['status18']) ? $_POST['status18'] : '';
			$status19 = isset($_POST['status19']) ? $_POST['status19'] : '';
			$status20 = isset($_POST['status20']) ? $_POST['status20'] : '';
			$status21 = isset($_POST['status21']) ? $_POST['status21'] : '';
			$status22 = isset($_POST['status22']) ? $_POST['status22'] : '';
			$status23 = isset($_POST['status23']) ? $_POST['status23'] : '';
			$status24 = isset($_POST['status24']) ? $_POST['status24'] : '';
			$status25 = isset($_POST['status25']) ? $_POST['status25'] : '';
			$status26 = isset($_POST['status26']) ? $_POST['status26'] : '';
			$status27 = isset($_POST['status27']) ? $_POST['status27'] : '';
			$status28 = isset($_POST['status28']) ? $_POST['status28'] : '';
			$status29 = isset($_POST['status29']) ? $_POST['status29'] : '';
			$status30 = isset($_POST['status30']) ? $_POST['status30'] : '';
			$status31 = isset($_POST['status31']) ? $_POST['status31'] : '';
			$status32 = isset($_POST['status32']) ? $_POST['status32'] : '';

			
			$sql = "SELECT *
					FROM patient
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);
			
			$sql = "SELECT *
					FROM dental_chart
					WHERE chart_id = :ch";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':ch', $chart);

			oci_execute($stid);

			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st1, '1',:tc1, :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st1', $status1);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st2, '2', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st2', $status2);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st3, '3', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st3', $status3);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st4, '4', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st4', $status4);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st5, '5', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st5', $status);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st6, '6', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st6', $status6);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st7, '7', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st7', $status7);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st8, '8', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st8', $status8);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st9, '9', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st9', $status9);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st10, '10', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st10', $status10);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st11, '11', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st11', $status11);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st12, '12', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st12', $status12);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st13, '13', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st13', $status13);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st14, '14', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st14', $status14);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st15, '15', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st15', $status15);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st16, '16', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st16', $status);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st17, '17', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st17', $status17);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st18, '18', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st18', $status18);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st19, '19', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st19', $status19);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st20, '20', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st20', $status20);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);

			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st21, '21', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st21', $status21);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st22, '22', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st22', $status22);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st23, '23', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st23', $status23);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st24, '24', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st24', $status24);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st25, '25', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st25', $status25);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st26, '26', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st26', $status26);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st27, '27', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st27', $status27);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st28, '28', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st28', $status28);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st29, '29', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st29', $status29);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st30, '30', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st30', $status30);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);

			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st31, '31', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st31', $status31);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( :tr, :st32, '32', :ch, :pi)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tr', $teeth_records);
			oci_bind_by_name($stid, ':st32', $status32);
			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':ch', $chart);


			oci_execute($stid);
			    header("Location: action_chart.php");
		
			oci_close($conn);
		}
		?>
		
<form class="container" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post" id="needs-validation" novalidate>

<div class="row">
<div class="col-md-1 mb-1">
	  <label> Record No. : </label>
      <input type="text" name="records" class="form-control" id="validationCustom01" style="text-transform: Capitalize" placeholder="" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	</div>

	<br><br>

<div class="row">
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes1" class="form-control" id="validationCustom05" value="1" maxlength="2" disabled>
      <input type="text" name="status1" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes2" class="form-control" id="validationCustom05" value="2" maxlength="2" disabled>
	  <input type="text" name="status2" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes3" class="form-control" id="validationCustom05" value="3" maxlength="2" disabled>
      <input type="text" name="status3" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes4" class="form-control" id="validationCustom05" value="4" maxlength="2" disabled>
	  <input type="text" name="status4" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes5" class="form-control" id="validationCustom05" value="5" maxlength="2" disabled>
      <input type="text" name="status5" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes6" class="form-control" id="validationCustom05" value="6" maxlength="2" disabled>
	  <input type="text" name="status6" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes7" class="form-control" id="validationCustom05" value="7" maxlength="2" disabled>
      <input type="text" name="status7" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes8" class="form-control" id="validationCustom05" value="8" maxlength="2" disabled>
	  <input type="text" name="status8" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
  </div> 
  <br><br>
<div class="row">
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes9" class="form-control" id="validationCustom05" value="9" maxlength="2" disabled>
      <input type="text" name="status9" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes10" class="form-control" id="validationCustom05" value="10" maxlength="2" disabled>
	  <input type="text" name="status10" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes11" class="form-control" id="validationCustom05" value="11" maxlength="2" disabled>
      <input type="text" name="status11" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes12" class="form-control" id="validationCustom05" value="12" maxlength="2" disabled>
	  <input type="text" name="status12" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes13" class="form-control" id="validationCustom05" value="13" maxlength="2" disabled>
      <input type="text" name="status13" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes14" class="form-control" id="validationCustom05" value="14" maxlength="2" disabled>
	  <input type="text" name="status14" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes15" class="form-control" id="validationCustom05" value="15" maxlength="2" disabled>
      <input type="text" name="status15" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes16" class="form-control" id="validationCustom05" value="16" maxlength="2" disabled>
	  <input type="text" name="status16" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
  </div> 
  <br><br>
<div class="row">
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes17" class="form-control" id="validationCustom05" value="17" maxlength="2" disabled>
      <input type="text" name="status17" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes18" class="form-control" id="validationCustom05" value="18" maxlength="2" disabled>
	  <input type="text" name="status18" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes19" class="form-control" id="validationCustom05" value="19" maxlength="2" disabled>
      <input type="text" name="status19" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes20" class="form-control" id="validationCustom05" value="20" maxlength="2" disabled>
	  <input type="text" name="status20" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes21" class="form-control" id="validationCustom05" value="21" maxlength="2" disabled>
      <input type="text" name="status21" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes22" class="form-control" id="validationCustom05" value="22" maxlength="2" disabled>
	  <input type="text" name="status22" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes23" class="form-control" id="validationCustom05" value="23" maxlength="2" disabled>
      <input type="text" name="status23" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes24" class="form-control" id="validationCustom05" value="24" maxlength="2" disabled>
	  <input type="text" name="status24" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
  </div> 
  <br><br>
<div class="row">
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes25" class="form-control" id="validationCustom05" value="25" maxlength="2" disabled>
      <input type="text" name="status25" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes26" class="form-control" id="validationCustom05" value="26" maxlength="2" disabled>
	  <input type="text" name="status26" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes27" class="form-control" id="validationCustom05" value="27" maxlength="2" disabled>
      <input type="text" name="status27" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes28" class="form-control" id="validationCustom05" value="28" maxlength="2" disabled>
	  <input type="text" name="status28" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes29" class="form-control" id="validationCustom05" value="29" maxlength="2" disabled>
      <input type="text" name="status29" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes30" class="form-control" id="validationCustom05" value="30" maxlength="2" disabled>
	  <input type="text" name="status30" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes31" class="form-control" id="validationCustom05" value="31" maxlength="2" disabled>
      <input type="text" name="status31" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes32" class="form-control" id="validationCustom05" value="32" maxlength="2" disabled>
	  <input type="text" name="status32" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
  </div> 
  <br><br>
  <div class="btn-group" role="group">
  <button class="btn btn-primary mb-2" type="submit" name="add"><i class ="fa fa-plus" aria-hidden="true"></i></button>
  <a class="btn btn-default mb-2" type="submit"  href="table_treatment.php" ><i class ="fa fa-arrow-left" aria-hidden="true"></i></a>
  </div>
  </div>
</form>
		<!--end add form-->

				</div>
				</div>
        </div>
      </div>
    </div>

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('needs-validation');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
})();
</script>
	
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>