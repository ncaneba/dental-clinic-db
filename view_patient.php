<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Patient Details</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
	      <!-- Search Tab--
		 <form class="form-inline my-2 my-lg-0 mr-lg-2" action="search_patient.php?search=" method="get">
			<div class="input-group">
              <input class="form-control" name="search" placeholder="Search..." style="text-transform: capitalize" type="text">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-primary">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
			<li class = "btn btn-space"><a href="insert_patient.php" class="btn btn-primary" ><i class ="fa fa-plus" aria-hidden="true"></i></a></li>
			<li class = "btn btn-space"><a href="table_patient.php"><i  class="fa fa-refresh" aria-hidden="true"></i></a></li>
          </form>-->

		  <br>
		
<?php

// Connects to the XE service (i.e. database) on the "localhost" machine
$conn = oci_connect('dental', 'dental', 'localhost/XE');
if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

$patientid = $_GET['patientid'];

			$sql = "SELECT *
					FROM patient
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);
			
			oci_fetch($stid);
			
			$patientfname = oci_result($stid, 'PFIRST_NAME');
			$patientlname = oci_result($stid, 'PLAST_NAME');
			$patientage = oci_result($stid, 'AGE');
			$patientcontact = oci_result($stid, 'CONTACT_NUM');
			$patientaddress = oci_result($stid, 'ADDRESS');


echo '
<p><h1>Patient Name: '.$patientlname.', '.$patientfname.'</h1></p>
<p><h1>Address: '.$patientaddress.'</h1></p>
<p><h1>Age: '.$patientage.'</h1></p>
<p><h1>Conntact No. +63'.$patientcontact.'</h1></p>
';

oci_close($conn);

?>
<br>
<h4>Back : <a href="table_checkup.php" class="btn btn-primary"><i class ="fa fa-arrow-left" aria-hidden="true"></i></a></h4>
				</div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>