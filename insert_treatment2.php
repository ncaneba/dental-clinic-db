
		<?php
		if(isset($_POST['add'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$treatmenttype = isset($_POST['treatmenttype']) ? $_POST['treatmenttype'] : '';

			$sql = "INSERT INTO treatment (treatment_id, treatment_type)
					VALUES (seq_treatment.NEXTVAL, :tt)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tt', $treatmenttype);

			oci_execute($stid);
			    header("Location: table_treatment.php");
				
				echo ''.$treatmenttype.'';
		
			oci_close($conn);
		}
		?>
