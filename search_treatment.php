<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Manage Payments</li>
      </ol>
      <div class="row">
        <div class="col-12">

		<!--search operation-->

	      <!-- Search Tab-->
	       <form class="form-inline my-2 my-lg-0 mr-lg-2" action="search_treatment.php" method="get">
            <div class="input-group">
              <input class="form-control" name="search" placeholder="Search..." type="text">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-primary">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
			<li class = "btn btn-space"><a href="insert_treatment.php" class="btn btn-primary" ><i class ="fa fa-plus" aria-hidden="true"></i></a></li>
			<li class = "btn btn-space"><a href="table_treatment.php"><i  class="fa fa-refresh" aria-hidden="true"></i></a></li>
          </form>
		  <br>	

		<?php
		if(isset($_GET['search'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$searchdata = $_GET['search'];
			$search = '%'.$searchdata.'%';

			$sql = "SELECT *
					FROM treatment
					WHERE treatment_id LIKE :sa OR treatment_type LIKE :sa OR price LIKE :sa";
					
			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':sa', $search);
			

			oci_execute($stid);

echo '
<div class="table-responsive">
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
<thread>
				  <tr class="active">
					  <th>Treatment ID</th>
					  <th>Type of Treatment</th>
					  <th>Actions</th>
				  </tr></thread>';
while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
    echo "<tr>\n";
    foreach ($row as $item) {
        echo "    <td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
    }
				echo '<td><div class="btn-group" role="group"><a href="edit_treatment.php?treatmentid='.htmlentities($row['TREATMENT_ID']).'" class="btn btn-primary"><i class ="fa fa-pencil" aria-hidden="true"></i></a>
				  <a class="btn btn-danger" href="delete_treatment.php?treatmentid='.htmlentities($row['TREATMENT_ID']).'" onclick=\'return confirm("Are you sure you want to delete this record?");\'><i class ="fa fa-trash-o" aria-hidden="true"></i></div></td>';
   echo "</tr>\n";
}
echo "</table>\n";
echo "</div>\n";

			oci_close($conn);
		}
		?>
		<!-- End search -->

				</div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>