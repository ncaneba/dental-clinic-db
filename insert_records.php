<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>
<link href="css/example.css" rel="stylesheet">
<!-- <link href="css/custom.sass" rel="stylesheet"> -->


<?php include('header.php') ?>


  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Add Teeth Code</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
		<!--add operation-->
		<?php
		if(isset($_POST['add'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			
			$status1 = isset($_POST['status1']) ? $_POST['status1'] : '';
			$status2 = isset($_POST['status2']) ? $_POST['status2'] : '';
			$status3 = isset($_POST['status3']) ? $_POST['status3'] : '';
			$status4 = isset($_POST['status4']) ? $_POST['status4'] : '';
			$status5 = isset($_POST['status5']) ? $_POST['status5'] : '';
			$status6 = isset($_POST['status6']) ? $_POST['status6'] : '';
			$status7 = isset($_POST['status7']) ? $_POST['status7'] : '';
			$status8 = isset($_POST['status8']) ? $_POST['status8'] : '';
			$status9 = isset($_POST['status9']) ? $_POST['status9'] : '';
			$status10 = isset($_POST['status10']) ? $_POST['status10'] : '';
			$status11 = isset($_POST['status11']) ? $_POST['status11'] : '';
			$status12 = isset($_POST['status12']) ? $_POST['status12'] : '';
			$status13 = isset($_POST['status13']) ? $_POST['status13'] : '';
			$status14 = isset($_POST['status14']) ? $_POST['status14'] : '';
			$status15 = isset($_POST['status15']) ? $_POST['status15'] : '';
			$status16 = isset($_POST['status16']) ? $_POST['status16'] : '';
			$status17 = isset($_POST['status17']) ? $_POST['status17'] : '';
			$status18 = isset($_POST['status18']) ? $_POST['status18'] : '';
			$status19 = isset($_POST['status19']) ? $_POST['status19'] : '';
			$status20 = isset($_POST['status20']) ? $_POST['status20'] : '';
			$status21 = isset($_POST['status21']) ? $_POST['status21'] : '';
			$status22 = isset($_POST['status22']) ? $_POST['status22'] : '';
			$status23 = isset($_POST['status23']) ? $_POST['status23'] : '';
			$status24 = isset($_POST['status24']) ? $_POST['status24'] : '';
			$status25 = isset($_POST['status25']) ? $_POST['status25'] : '';
			$status26 = isset($_POST['status26']) ? $_POST['status26'] : '';
			$status27 = isset($_POST['status27']) ? $_POST['status27'] : '';
			$status28 = isset($_POST['status28']) ? $_POST['status28'] : '';
			$status29 = isset($_POST['status29']) ? $_POST['status29'] : '';
			$status30 = isset($_POST['status30']) ? $_POST['status30'] : '';
			$status31 = isset($_POST['status31']) ? $_POST['status31'] : '';
			$status32 = isset($_POST['status32']) ? $_POST['status32'] : '';

			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.NEXTVAL, :st1, '1', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st1', $status1);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st2, '2', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st2', $status2);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st3, '3', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st3', $status3);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st4, '4', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st4', $status4);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st5, '5', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st5', $status5);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st6, '6', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st6', $status6);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st7, '7', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st7', $status7);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st8, '8', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st8', $status8);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st9, '9', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st9', $status9);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st10, '10', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st10', $status10);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st11, '11', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st11', $status11);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st12, '12', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st12', $status12);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st13, '13', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st13', $status13);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st14, '14', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st14', $status14);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st15, '15', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st15', $status15);

			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st16, '16', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st16', $status);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st17, '17', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st17', $status17);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st18, '18', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st18', $status18);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st19, '19', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st19', $status19);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st20, '20', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st20', $status20);

			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st21, '21', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st21', $status21);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st22, '22', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st22', $status22);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st23, '23', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st23', $status23);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st24, '24', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st24', $status24);

			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st25, '25', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st25', $status25);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st26, '26', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st26', $status26);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st27, '27', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st27', $status27);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st28, '28', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st28', $status28);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st29, '29', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st29', $status29);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st30, '30', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st30', $status30);

			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st31, '31', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st31', $status31);


			oci_execute($stid);
			$sql = "INSERT INTO teeth_record (record_no, status, teeth_code, chart_id, patient_id)
					VALUES ( seq_records.CURRVAL, :st32, '32', seq_chart.CURRVAL, seq_patient.CURRVAL)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':st32', $status32);


			oci_execute($stid);
			
			$sql = "SELECT patient_id FROM patient ORDER BY patient_id DESC";
			
			$stid = oci_parse($conn, $sql);
			
			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);
			
			oci_fetch($stid);
  
			$patientid = oci_result($stid, 'PATIENT_ID');
			
			    header("Location: table_chart.php?patientid=$patientid");
		
			oci_close($conn);
		}
		?>
	<?php
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$patientid = isset($_GET['patientid']) ? $_GET['patientid'] : '';
			$chart = isset($_GET['chartid']) ? $_GET['chartid'] : '';

			$sql = "SELECT *
					FROM patient
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);
  
			oci_fetch($stid);
			
			$pfirstname = oci_result($stid, 'PFIRST_NAME');
			$plastname = oci_result($stid, 'PLAST_NAME');
			
			$sql = "SELECT *
					FROM dental_chart
					WHERE chart_id = :ch
					ORDER BY chart_id DESC";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':ch', $chart);

			oci_execute($stid);
			
			oci_fetch($stid);
			
			$chart = oci_result($stid, 'CHART_ID');

			echo '<form class="container" action="" method="post" id="needs-validation" novalidate>
<div>
<h2>
<lable>Dental Chart:</label>
</h2>
<br><br>
<h4>
<label>Chart No.: '.$chart.'</label>
<br>
<label>Name :'.$plastname.',</label>
<label>'.$pfirstname.'</label>
<br>
</h4>
</div>

	<br><br>

<div class="row">
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes1" class="form-control" id="validationCustom05" value="1" maxlength="2" disabled>
      <input type="text" name="status1" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes2" class="form-control" id="validationCustom05" value="2" maxlength="2" disabled>
	  <input type="text" name="status2" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes3" class="form-control" id="validationCustom05" value="3" maxlength="2" disabled>
      <input type="text" name="status3" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes4" class="form-control" id="validationCustom05" value="4" maxlength="2" disabled>
	  <input type="text" name="status4" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes5" class="form-control" id="validationCustom05" value="5" maxlength="2" disabled>
      <input type="text" name="status5" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes6" class="form-control" id="validationCustom05" value="6" maxlength="2" disabled>
	  <input type="text" name="status6" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes7" class="form-control" id="validationCustom05" value="7" maxlength="2" disabled>
      <input type="text" name="status7" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes8" class="form-control" id="validationCustom05" value="8" maxlength="2" disabled>
	  <input type="text" name="status8" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
  </div> 
  <br><br>
<div class="row">
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes9" class="form-control" id="validationCustom05" value="9" maxlength="2" disabled>
      <input type="text" name="status9" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes10" class="form-control" id="validationCustom05" value="10" maxlength="2" disabled>
	  <input type="text" name="status10" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes11" class="form-control" id="validationCustom05" value="11" maxlength="2" disabled>
      <input type="text" name="status11" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes12" class="form-control" id="validationCustom05" value="12" maxlength="2" disabled>
	  <input type="text" name="status12" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes13" class="form-control" id="validationCustom05" value="13" maxlength="2" disabled>
      <input type="text" name="status13" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes14" class="form-control" id="validationCustom05" value="14" maxlength="2" disabled>
	  <input type="text" name="status14" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes15" class="form-control" id="validationCustom05" value="15" maxlength="2" disabled>
      <input type="text" name="status15" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes16" class="form-control" id="validationCustom05" value="16" maxlength="2" disabled>
	  <input type="text" name="status16" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
  </div> 
  <br><br>
<div class="row">
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes17" class="form-control" id="validationCustom05" value="17" maxlength="2" disabled>
      <input type="text" name="status17" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes18" class="form-control" id="validationCustom05" value="18" maxlength="2" disabled>
	  <input type="text" name="status18" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes19" class="form-control" id="validationCustom05" value="19" maxlength="2" disabled>
      <input type="text" name="status19" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes20" class="form-control" id="validationCustom05" value="20" maxlength="2" disabled>
	  <input type="text" name="status20" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes21" class="form-control" id="validationCustom05" value="21" maxlength="2" disabled>
      <input type="text" name="status21" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes22" class="form-control" id="validationCustom05" value="22" maxlength="2" disabled>
	  <input type="text" name="status22" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes23" class="form-control" id="validationCustom05" value="23" maxlength="2" disabled>
      <input type="text" name="status23" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes24" class="form-control" id="validationCustom05" value="24" maxlength="2" disabled>
	  <input type="text" name="status24" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
  </div> 
  <br><br>
<div class="row">
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes25" class="form-control" id="validationCustom05" value="25" maxlength="2" disabled>
      <input type="text" name="status25" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes26" class="form-control" id="validationCustom05" value="26" maxlength="2" disabled>
	  <input type="text" name="status26" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes27" class="form-control" id="validationCustom05" value="27" maxlength="2" disabled>
      <input type="text" name="status27" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes28" class="form-control" id="validationCustom05" value="28" maxlength="2" disabled>
	  <input type="text" name="status28" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes29" class="form-control" id="validationCustom05" value="29" maxlength="2" disabled>
      <input type="text" name="status29" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes30" class="form-control" id="validationCustom05" value="30" maxlength="2" disabled>
	  <input type="text" name="status30" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes31" class="form-control" id="validationCustom05" value="31" maxlength="2" disabled>
      <input type="text" name="status31" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-1 mb-1">
      <input type="text" name="codes32" class="form-control" id="validationCustom05" value="32" maxlength="2" disabled>
	  <input type="text" name="status32" class="form-control" id="validationCustom05" style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
  </div> 
  <br><br>
  <div class="btn-group" role="group">
  <button class="btn btn-primary mb-2" type="submit" name="add"><i class ="fa fa-plus" aria-hidden="true"></i></button>';

			oci_close($conn);
		?>
		
		
				<?php
		if(isset($_GET['patientid'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$patientid = $_GET['patientid'];

			$sql = "SELECT *
					FROM dental_chart
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);
			
			if ($line = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
    	
				echo '<a class="btn btn-default mb-2" type="submit"  href="table_chart.php?patientid='.htmlentities($line['PATIENT_ID']).'" ><i class ="fa fa-arrow-left" aria-hidden="true"></i></a>
  </div>
  </div>
</form>';
		}	
				
			oci_close($conn);
		}
		?>
		
		<!--end add form-->
<form class="container">
<table style="width:100%">
<b>Legends:</b>
<br>
<tr>
<th>Condition</th>
<th>Restoration & Prosthetics</th>
<th>Surgery</th>
</tr>
<tr>
<td>D - Decayed (Caries indicated for Filling)</td>
<td>J - Jacket Crown</td>
<td>X - Extraction due to Caries</td>
</tr>
<tr>
<td>M - Missing due to Caries</td>
<td>A - Arnaigam Filling</td>
<td>XO - Extraction due to Other Causes</td>
</tr>
<tr>
<td>F - Filled</td>
<td>AB - Abutment</td>
<td>/ - Present Teeth</td>
</tr>
<tr>
<td>I - Caries Indicated for Extraction</td>
<td>P - Pontic</td>
<td>CM - Congenitally Missing</td>
</tr>
<tr>
<td>RF - Root Fragment</td>
<td>IN - Inlay</td>
<td>SP - Supernumerary</td>
</tr>
<tr>
<td>MO - Missing due to Other Causes</td>
<td>FX - Fixed Cure Composite</td>
</tr>
<tr>
<td>IM - Impacted Tooth</td>
<td>RM - Removable Denture</td>
</tr>
<td></td>
</table>
</form>

				</div>
				</div>
        </div>
      </div>
    </div>

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('needs-validation');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
})();
</script>
	
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>