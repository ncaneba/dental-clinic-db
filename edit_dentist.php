<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Add Dentist</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
		<!--update operation-->
		<?php
		if(isset($_POST['edit'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}
  
			$doctorid = isset($_GET['doctorid']) ? $_GET['doctorid'] : '';
  
			$sql = "SELECT *
					FROM dentist
					WHERE doctor_id = :di";
  
			$stid = oci_parse($conn, $sql);
  
			oci_bind_by_name($stid, ':di', $doctorid);
  
			oci_execute($stid);
  
			oci_fetch($stid);
  
			$oldfirstname = oci_result($stid, 'FNAME');
			$oldmiddleinitial = oci_result($stid, 'MINITIAL');
			$oldlastname = oci_result($stid, 'LNAME');
			$oldcontactno = oci_result($stid, 'CONTACT_NUMBER');
  
			$firstname = !empty($_POST['firstname']) ? $_POST['firstname'] : $oldfirstname;
			$middleinitial = !empty($_POST['middleinitial']) ? $_POST['middleinitial'] : $oldmiddleinitial;
			$lastname = !empty($_POST['lastname']) ? $_POST['lastname'] : $oldlastname;
			$contactno = !empty($_POST['contactno']) ? $_POST['contactno'] : $oldcontactno;
			$sql = "UPDATE dentist
					SET fname = :fn, minitial = :mi, lname = :ln, contact_number = :cn
					WHERE doctor_id = :di";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':di', $doctorid);
			oci_bind_by_name($stid, ':fn', $firstname);
			oci_bind_by_name($stid, ':mi', $middleinitial);
			oci_bind_by_name($stid, ':ln', $lastname);
			oci_bind_by_name($stid, ':cn', $contactno);

			oci_execute($stid);
		    header("Location: table_dentist.php");

			oci_close($conn);
		}
		?>
		<!--end update operation-->
		<!--update form-->
		<?php
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$doctorid = $_GET['doctorid'];

			$sql = "SELECT *
					FROM dentist
					WHERE doctor_id = :di";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':di', $doctorid);

			oci_execute($stid);
  
			oci_fetch($stid);

			$firstname = oci_result($stid, 'FNAME');
			$middleinitial = oci_result($stid, 'MINITIAL');
			$lastname = oci_result($stid, 'LNAME');
			$contactno = oci_result($stid, 'CONTACT_NUMBER');
  
			echo '<div class="container">
<form class="container" action="" method="post" id="needs-validation" novalidate>
  <div class="row">
    <div class="col-md-5 mb-6">
      <label for="validationCustom01">First name</label>
      <input type="text" name="firstname" class="form-control" value = '.$firstname.' id="validationCustom01" style="text-transform: capitalize" placeholder="First name" required>
	  <div class="invalid-feedback">
        This is required.
      </div>
    </div>
	<div class="col-md-5 mb-6">
      <label for="validationCustom01">Middle Initial</label>
      <input type="text" name="middleinitial" class="form-control" value = '.$middleinitial.' id="validationCustom01" style="text-transform: capitalize" placeholder="MI" maxlength="2" required>
	  <div class="invalid-feedback">
        This is required.
      </div>
    </div>
    <div class="col-md-5 mb-6">
      <label for="validationCustom02">Last name</label>
      <input type="text" name="lastname" class="form-control" value = '.$lastname.' id="validationCustom02" style="text-transform: capitalize" placeholder="Last name" required>
	  <div class="invalid-feedback">
        This is required.
      </div>
    </div>
    <div class="col-md-5 mb-">
      <label for="validationCustom05">Contact No.</label>
      <input type="text" name="contactno" class="form-control" value = '.$contactno.' id="validationCustom05" maxlength="11" onkeypress="return isNumber(event)" placeholder="09XXXXXXXXX" required>
      <div class="invalid-feedback">
        This is required
      </div>
    </div>
  </div>
  </div>
	<button class="btn btn-primary" type="submit" name="edit"><i class="fa fa-pencil aria-hidden="true"></i></button>
</form>
			<form action="table_dentist.php" method="post">
			 <button type="submit" class="btn btn-default">Return</button>
			</form>
				  </div>';

			oci_close($conn);
		?>
		<!--end update form-->

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('needs-validation');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
})();
</script>

<script type="text/javascript"> 
// Only Number Input    
function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if ( (charCode > 31 && charCode < 48) || charCode > 57) {
            return false;
        }
        return true;
    }
</script>
	
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>