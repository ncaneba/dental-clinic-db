<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Edit Treatment</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
		<!--update operation-->
		<?php
		if(isset($_POST['edit'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}
  
			$treatmentid = isset($_GET['treatmentid']) ? $_GET['treatmentid'] : '';
  
			$sql = "SELECT *
					FROM treatment
					WHERE treatment_id = :ti";
  
			$stid = oci_parse($conn, $sql);
  
			oci_bind_by_name($stid, ':ti', $treatmentid);
  
			oci_execute($stid);
  
			oci_fetch($stid);
  
			$oldtreatmentid = oci_result($stid, 'TREATMENT_ID');
			$oldtreatmenttype = oci_result($stid, 'TREATMENT_TYPE');
			$oldprice = oci_result($stid, 'PRICE');
  
			$treatmentid = !empty($_POST['treatmentid']) ? $_POST['treatmentid'] : $oldtreatmentid;
			$treatmenttype = !empty($_POST['treatmenttype']) ? $_POST['treatmenttype'] : $oldtreatmenttype;
			$price = !empty($_POST['price']) ? $_POST['price'] : $oldprice;
			$sql = "UPDATE treatment
					SET treatment_type = :tt, price = :pr
					WHERE treatment_id = :ti";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':ti', $treatmentid);
			oci_bind_by_name($stid, ':tt', $treatmenttype);
			oci_bind_by_name($stid, ':pr', $price);

			oci_execute($stid);
		    header("Location: table_treatment.php");

			oci_close($conn);
		}
		?>
		<!--end update operation-->
		<!--update form-->
		<?php
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$treatmentid = $_GET['treatmentid'];

			$sql = "SELECT *
					FROM treatment
					WHERE treatment_id = :ti";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':ti', $treatmentid);

			oci_execute($stid);
  
			oci_fetch($stid);

			$treatmenttype = oci_result($stid, 'TREATMENT_TYPE');
			$price = oci_result($stid, 'PRICE');
  
			echo '<div class="container">
<form class="form-inline" action="" method="post" id="needs-validation" novalidate>
  <div class="form-group mx-sm-3 mb-2">
    <label class="sr-only">Type of Treatment</label>
    <input type="text" name="treatmenttype" value="'.$treatmenttype.'"class="form-control" placeholder="Type of Treatment" >
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <label class="sr-only">Price</label>
    <input type="text" name="price" value="'.$price.'"class="form-control" placeholder="Price">
  </div>
  <div class="btn-group" role="group">
  <button class="btn btn-primary mb-2" type="submit" name="edit"><i class ="fa fa-pencil" aria-hidden="true"></i></button>
  <button class="btn btn-default mb-2" type="submit"  href="table_treatment.php" ><i class ="fa fa-arrow-left" aria-hidden="true"></i></button>
  </div>
</form>

				  </div>
				  </div>';

			oci_close($conn);
		?>
		<!--end update form-->

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('needs-validation');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
})();
</script>

<script type="text/javascript"> 
// Only Number Input    
function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if ( (charCode > 31 && charCode < 48) || charCode > 57) {
            return false;
        }
        return true;
    }
</script>
	
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>