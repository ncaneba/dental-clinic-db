<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">List of Treatments</li>
      </ol>
      <div class="row">
        <div class="col-12">
		<?php include('insert_treatment2.php') ?>
	      <!-- Search Tab-->
		  
	       <form class="form-inline my-2 my-lg-0 mr-lg-2" action="search_treatment.php" method="post">
            <div class="input-group">
              <input class="form-control" name="patientid" placeholder="Search..." type="text">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-primary">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
		  
<form class="form-inline " action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post" id="needs-validation" novalidate>
  <div class="form-group mx-sm-3 mb-2">
    <label class="sr-only">Type of Treatment</label>
    <input type="text" name="treatmenttype" class="form-control" placeholder="Type of Treatment">
  </div>
  <div class="btn-group" role="group">
  <button class="btn btn-primary mb-2" type="submit" name="add"><i class ="fa fa-plus" aria-hidden="true"></i></button>
  </div>
</form>
		  <br>	
		
		
<?php

// Connects to the XE service (i.e. database) on the "localhost" machine
$conn = oci_connect('dental', 'dental', 'localhost/XE');
if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

$stid = oci_parse($conn, 'SELECT * FROM treatment ORDER BY treatment_id ASC');
oci_execute($stid);

echo '<table class="table table-bordered" cellspacing="0">
<thread>
				  <tr class="active">
					  <th>Treatment ID</th>
					  <th>Type of Treatment</th>
					  <th>Actions</th>
				  </tr></thread>';
while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
    echo "<tr>\n";
    foreach ($row as $item) {
        echo "    <td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
    }
				echo '<td><div class="btn-group" role="group"><a href="edit_treatment.php?treatmentid='.htmlentities($row['TREATMENT_ID']).'" class="btn btn-primary"><i class ="fa fa-pencil" aria-hidden="true"></i></a>
				  <a class="btn btn-danger" href="delete_treatment.php?treatmentid='.htmlentities($row['TREATMENT_ID']).'" onclick=\'return confirm("Are you sure you want to delete this record?");\'><i class ="fa fa-trash-o" aria-hidden="true"></i></div></td>';
    echo "</tr>\n";
}
echo "</table>\n";

?>

				</div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>