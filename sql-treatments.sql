INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Consultation','350');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Oral Prophylaxie: Light/Moderate','600');


INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Oral Prophylaxie: Heavy','1500');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Flouride Treatment','1500');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Restorations: Temporary','600');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Restorations: Light Cured - Class 1(Simple)','600');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Restorations: Light Cured - Class 2','1000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Restorations: Light Cured - Class 3','600');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Restorations: Light Cured - Class 4','1000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Restorations: Light Cured - Class 5','1000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Restorations: Light Cured - Modified(Complicated)','1500');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Restorations: Amalgam - Simple','600');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Restorations: Amalgam - Complicated','800');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Pit and Fissure Sealant','600');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Extraction: Incisors','600');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Extraction: Canine to Molars','800');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Odontectomy','10000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Root Canal Treatment: Mono Rooted','5000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Root Canal Treatment: Additional Canal','2000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Crowns: Plastic','3000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Crowns: Porcelain','6000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Removable Ortho Apliances U/L','10000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Orthodontic Treatment: Conventional','50000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Orthodontic Treatment: Self-ligating','80000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Removable Partial Denture: One Piece Casted','10000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Removable Partial Denture: Stay Plate(1 Unit)','2500');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Removable Partial Denture: Stay Plate(1 Unit) - Plastic','600');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Removable Partial Denture: Stay Plate(1 Unit) - Porcelain','1000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Removable Partial Denture: Thermoplastic - Unilateral','8000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Removable Partial Denture: Thermoplastic - Bilateral','16000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Removable Partial Denture: One-Piece/Thermoplastic Combination','25000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Complete Denture(Upper/Lower): Plastic Pontics - Lee','12000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Complete Denture(Upper/Lower): Plastic Pontics - Natura Tone','15000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Complete Denture(Upper/Lower): Porcelain Pontics China','15000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Complete Denture(Upper/Lower): Porcelain Pontics China - Natura Dent','20000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Denture Repair(Simple)','1000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Rebasing Per Arch','4000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Periodontal Treatment Per Quadrant','3500');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Radiograph: Periapical','400');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Radiograph: Cephalometric','1000');

INSERT INTO treatment (treatment_id, treatment_type, price)
VALUES (seq_treatment.NEXTVAL,'Radiograph: Panoramic','1000');

commit;