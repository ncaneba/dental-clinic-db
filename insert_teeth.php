<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Add Teeth Code</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
		<!--add operation-->
		<?php
		if(isset($_POST['add'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$teethname = isset($_POST['name']) ? $_POST['name'] : '';

			$sql = "INSERT INTO teeth (teeth_code, teeth_name)
					VALUES (seq_teeth.NEXTVAL, :tn)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tn', $teethname);

			oci_execute($stid);
			    header("Location: table_teeth.php");
		
			oci_close($conn);
		}
		?>
		

<form class="form-inline" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post" id="needs-validation" novalidate>
  <div class="form-group mx-sm-3 mb-2">
    <label class="sr-only">Teeth Name</label>
    <input type="text" name="name" class="form-control" placeholder="Teeth Name">
  </div>
  <div class="btn-group" role="group">
  <button class="btn btn-primary mb-2" type="submit" name="add"><i class ="fa fa-plus" aria-hidden="true"></i></button>
  <a class="btn btn-default mb-2" type="submit"  href="table_treatment.php" ><i class ="fa fa-arrow-left" aria-hidden="true"></i></a>
  </div>
</form>
  </div>
  <br>

    <!--<input type="submit" class="btn btn-primary" name="add" value="Add patient"> -->
</form>
		<!--end add form-->

				</div>
				</div>
        </div>
      </div>
    </div>

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('needs-validation');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
})();
</script>
	
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>