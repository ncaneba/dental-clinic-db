<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Add Treatment</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
		<!--add operation-->
		<?php
		if(isset($_POST['add'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}
  
			$patientid = isset($_GET['patientid']) ? $_GET['patientid'] : '';
			
  
			$sql = "SELECT *
					FROM patient
					WHERE patient_id = :pi";
  
			$stid = oci_parse($conn, $sql);
  
			oci_bind_by_name($stid, ':pi', $patientid);

			$treatmenttype = isset($_POST['treatmenttype']) ? $_POST['treatmenttype'] : '';
  
			oci_execute($stid);
  
			oci_fetch($stid);
  
			$pfirstname = oci_result($stid, 'PFIRST_NAME');
			$plastname = oci_result($stid, 'PLAST_NAME');
			$patientid = oci_result($stid, 'PATIENT_ID');
 
  
			$sql = "INSERT INTO treatment (treatment_id, treatment_type)
					VALUES (seq_treatment.NEXTVAL, :tt)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tt', $treatmenttype);
			
			

			oci_execute($stid);
		    header("Location: action_treatment.php");

			oci_close($conn);
		}
		?>
		
		
				<?php
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$patientid = $_GET['patientid'];

			$sql = "SELECT *
					FROM patient
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);

			
			oci_execute($stid);
  
			oci_fetch($stid);

			$pfirstname = oci_result($stid, 'PFIRST_NAME');
			$plastname = oci_result($stid, 'PLAST_NAME');
			$patientid = oci_result($stid, 'PATIENT_ID');
  
			echo '<div class="container">
<form class="container" action="#" method="post" id="needs-validation" novalidate>
  <div class="form-group">
	<h2>Patient Name: <b>'.$plastname.','.$pfirstname.'</b></h2>
  </div><br>
  <div class="row">
  <div class="form-group">
    <label for="validationCustom05">Type of Treatment</label>
    <select class="form-control" name="treatmenttype" id="validationCustom05">
      <option>EXAMINATION</option>
      <option>SCALING</option>
      <option>TEMP. FILLLG</option>
	  <option>PERM. FILLLG</option>
	  <option>I. ORAL HYG.</option>
	  <option>SEALANT</option>
	  <option>EXTRACTION</option>
	  <option>COMPLETION</option>
    </select>
  </div>
  </div>
  </div>
  <br>

    <!--<input type="submit" class="btn btn-primary" name="add" value="Add patient"> -->
	<button class="btn btn-primary" type="submit" name="add">Add</button>
</form>';

			oci_close($conn);
		?>
	<a class="btn btn-default" type="submit"  href="action_treatment.php" > Return </a>
		<!--end add form-->

				</div>
				</div>
        </div>
      </div>
    </div>

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('needs-validation');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
})();
</script>

<script type="text/javascript"> 
// Only Number Input    
function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if ( (charCode > 31 && charCode < 48) || charCode > 57) {
            return false;
        }
        return true;
    }
</script>
	
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>