<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Add Patient</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
		<!--update operation-->
		<?php
		if(isset($_POST['edit'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}
  
			$checkupid = isset($_GET['checkupid']) ? $_GET['checkupid'] : '';
  
			$sql = "SELECT *
					FROM check_up
					WHERE check_up_id = :ci";
  
			$stid = oci_parse($conn, $sql);
  
			oci_bind_by_name($stid, ':ci', $checkupid);
  
			oci_execute($stid);
  
			oci_fetch($stid);
  
			$oldcheckupdate = oci_result($stid, 'CHECK_UP_DATE');
			$oldcheckuptime = oci_result($stid, 'CHECK_UP_TIME');
			$oldcheckupdetails = oci_result($stid, 'CHECK_UP_DETAILS');
  
			$checkupdate = !empty($_POST['checkupdate']) ? $_POST['checkupdate'] : $oldcheckupdate;
			$checkuptime = !empty($_POST['checkuptime']) ? $_POST['checkuptime'] : $oldcheckuptime;
			$checkupdetails = !empty($_POST['checkupdetails']) ? $_POST['checkupdetails'] : $oldcheckupdetails;

			$sql = "UPDATE check_up
					SET check_up_date = :cd, check_up_time = :ct, check_up_details = :de
					WHERE check_up_id = :ci";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':ci', $checkupid);
			oci_bind_by_name($stid, ':cd', $checkupdate);
			oci_bind_by_name($stid, ':ct', $checkuptime);
			oci_bind_by_name($stid, ':de', $checkupdetails);

			oci_execute($stid);
		    header("Location: table_checkup.php");

			oci_close($conn);
		}
		?>
		<!--end update operation-->
		<!--add form-->
	<?php
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$checkupid = isset($_GET['checkupid']) ? $_GET['checkupid'] : '';

			$sql = "SELECT *
					FROM check_up
					WHERE check_up_id = :ci";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':ci', $checkupid);

			oci_execute($stid);
  
			oci_fetch($stid);
			
			$checkupdate = oci_result($stid, 'CHECK_UP_DATE');
			$checkuptime = oci_result($stid, 'CHECK_UP_TIME');
			$checkupdetails = oci_result($stid, 'CHECK_UP_DETAILS');
			
				echo '
				<div class="container">
				<br><br>
				<form class="container" action="" method="post" id="needs-validation" novalidate>
				  <div class="row">
    <div class="col-md-4 mb-3">
      <label for="validationCustom01">Check-up Date</label>
      <input type="text" name="checkupdate" class="form-control" id="validationCustom01" value="'.$checkupdate.'" placeholder="" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-3 mb-3">
      <label for="validationCustom01">Check-up Time</label>
      <input type="text" onchange="onTimeChange()" id="time" value="'.$checkuptime.'"name="checkuptime" class="form-control"  >
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-5 mb-3">
      <label for="validationCustom02">Check-up details</label>
      <input type="text" name="checkupdetails" class="form-control" value="'.$checkupdetails.'" id="validationCustom02" placeholder="Details" required>
	  <div class="invalid-feedback">
      </div>
    </div>
  </div>
	<br>
  <div class="btn-group" role="group" class="col-md-5">
  <button class="btn btn-primary mb-2" onclick="print()" type="submit" name="edit"><i class ="fa fa-plus" aria-hidden="true"></i></button>
  <a class="btn btn-default mb-2" type="submit"  href="table_checkup.php" ><i class ="fa fa-arrow-left" aria-hidden="true"></i></a>
  </div>
  		</div>
</form>';

oci_close($conn);
		?>
		<!--end add form-->

				</div>
				</div>
        </div>
      </div>
    </div>

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('needs-validation');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
})();
</script>

<script type="text/javascript"> 
// Only Number Input    
function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if ( (charCode > 31 && charCode < 48) || charCode > 57) {
            return false;
        }
        return true;
    }
</script>

<script>



function onTimeChange() {
  var timeSplit = inputEle.value.split(':'),
    hours,
    minutes,
    meridian;
  hours = timeSplit[0];
  minutes = timeSplit[1];
  if (hours > 12) {
    meridian = 'PM';
    hours -= 12;
  } else if (hours < 12) {
    meridian = 'AM';
    if (hours == 0) {
      hours = 12;
    }
  } else {
    meridian = 'PM';
  }
  alert(hours + ':' + minutes + ' ' + meridian);
}
</script>

<script>
function print() {
  t = document.getElementById('time').value
  var [h,m] = t.split(":");
  console.log((h%12+12*(h%12==0))+":"+m, h >= 12 ? 'PM' : 'AM');
}
</script>
	
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>