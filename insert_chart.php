<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>
<!-- Insert Function-->

		<?php
		if(isset($_GET['patientid'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$patientid = $_GET['patientid'];

			$sql = "INSERT INTO dental_chart (chart_id, dates, patient_id)
					VALUES (seq_chart.NEXTVAL, SYSDATE, :pi)";

			$stid = oci_parse($conn, $sql);
			
			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);
			
			$sql = "SELECT chart_id FROM dental_chart WHERE patient_id = :pi ORDER BY chart_id DESC";
			
			$stid = oci_parse($conn, $sql);
			
			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);
			
			oci_fetch($stid);
  
			$chartid = oci_result($stid, 'CHART_ID');
			
			header("Location: insert_records.php?patientid=$patientid&&chartid=$chartid");
			
			oci_close($conn);
		}
		?>
		
<!-- End Insert Function-->