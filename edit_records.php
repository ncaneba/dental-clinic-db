<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>
<link href="css/example.css" rel="stylesheet">
<!-- <link href="css/custom.sass" rel="stylesheet"> -->

<style>

	ZAS
</style>

<?php include('header.php') ?>


  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Add Teeth Code</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
		<!--edit operation-->
		<?php
		if(isset($_POST['edit'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$teethid = isset($_GET['teethid']) ? $_GET['teethid'] : '';
			$recordno = isset($_GET['recordno']) ? $_GET['recordno'] : '';
			$patientid = isset($_GET['patientid']) ? $_GET['patientid'] : '';

			$sql = "SELECT *
					FROM patient
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);
			
			
			$sql = "SELECT *
					FROM teeth_record
					WHERE teeth_code = :tc
					AND record_no = :rn";
  
			$stid = oci_parse($conn, $sql);
  
			oci_bind_by_name($stid, ':tc', $teethid);
			oci_bind_by_name($stid, ':rn', $recordno);

			oci_execute($stid);
			
			oci_fetch($stid);

			$oldstatus = oci_result($stid, 'STATUS');
  
			$status = !empty($_POST['status']) ? $_POST['status'] : $oldstatus;
			
			$sql = "UPDATE teeth_record
					SET status = :st
					WHERE teeth_code = :tc
					AND record_no = :rn";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tc', $teethid);
			oci_bind_by_name($stid, ':rn', $recordno);
			oci_bind_by_name($stid, ':st', $status);
			
			oci_execute($stid);
			    header("Location: table_records.php?patientid=$patientid");
		
			oci_close($conn);
		}
		?>
	<?php
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$patientid = isset($_GET['patientid']) ? $_GET['patientid'] : '';

			$sql = "SELECT *
					FROM patient
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);
			
			$teethid = isset($_GET['teethid']) ? $_GET['teethid'] : '';

			$sql = "SELECT *
					FROM teeth
					WHERE teeth_code = :tc";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tc', $teethid);

			oci_execute($stid);
  
			oci_fetch($stid);
			
			$teethcode = oci_result($stid, 'TEETH_CODE');
			$teethname = oci_result($stid, 'TEETH_NAME');
			
			$recordno = isset($_GET['recordno']) ? $_GET['recordno'] : '';
			$teethid = isset($_GET['teethid']) ? $_GET['teethid'] : '';
			
			$sql = "SELECT *
					FROM teeth_record
					WHERE record_no = :rn
					AND teeth_code = :tc";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':rn', $recordno);
			oci_bind_by_name($stid, ':tc', $teethid);

			oci_execute($stid);
  
			oci_fetch($stid);
			
			$recordno = oci_result($stid, 'RECORD_NO');
			$status = oci_result($stid, 'STATUS');

			echo '<form class="container" action="" method="post" id="needs-validation" novalidate>
<div>
<h1>
<label>Teeth Name: '.$teethname.'</label>
<br>
</h1>
</div>
<div class="row">
<div class="col-md-1 mb-1">
	  <label> Record No. : </label>
      <input type="text" name="records" class="form-control" id="validationCustom01" value="'.$recordno.'" style="text-transform: Capitalize" placeholder="" disabled required>
	  <div class="invalid-feedback">
      </div>
    </div>
	</div>

	<br><br>

<div class="row">
    <div class="col-md-1 mb-1">
	  <input type="text" name="codes" class="form-control" id="validationCustom05" value="'.$teethcode.'" maxlength="2" disabled>
      <input type="text" name="status" class="form-control" id="validationCustom05" value="'.$status.'"style="text-transform: Capitalize" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
  </div>
    <br><br>
  <div class="btn-group" role="group">
  <button class="btn btn-primary mb-2" type="submit" name="edit"><i class ="fa fa-plus" aria-hidden="true"></i></button>
  <a class="btn btn-default mb-2" type="submit"  href="table_records.php?patientid='.$patientid.'" ><i class ="fa fa-arrow-left" aria-hidden="true"></i></a>
  </div>
</form>';

			oci_close($conn);
		?> 
		<!--end add form-->
<form class="container">
<table style="width:100%">
<b>Legends:</b>
<br>
<tr>
<th>Condition</th>
<th>Restoration & Prosthetics</th>
<th>Surgery</th>
</tr>
<tr>
<td>D - Decayed (Caries indicated for Filling)</td>
<td>J - Jacket Crown</td>
<td>X - Extraction due to Caries</td>
</tr>
<tr>
<td>M - Missing due to Caries</td>
<td>A - Arnaigam Filling</td>
<td>XO - Extraction due to Other Causes</td>
</tr>
<tr>
<td>F - Filled</td>
<td>AB - Abutment</td>
<td>/ - Present Teeth</td>
</tr>
<tr>
<td>I - Caries Indicated for Extraction</td>
<td>P - Pontic</td>
<td>CM - Congenitally Missing</td>
</tr>
<tr>
<td>RF - Root Fragment</td>
<td>IN - Inlay</td>
<td>SP - Supernumerary</td>
</tr>
<tr>
<td>MO - Missing due to Other Causes</td>
<td>FX - Fixed Cure Composite</td>
</tr>
<tr>
<td>IM - Impacted Tooth</td>
<td>RM - Removable Denture</td>
</tr>
<td></td>
</table>
</form>
				</div>
				</div>
        </div>
      </div>
    </div>

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('needs-validation');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
})();
</script>
	
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>