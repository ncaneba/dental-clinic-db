<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">About Us</li>
      </ol>
      <!-- End Breadcrumbs-->
	  
	  <!-- About Us-->

	<div>
	<h1>
		<span class="fa fa-fw fa-info-circle fa-3x" style=" vertical-align: middle;"></span>
		<span class="my-text">About Us: </span>
	</h1>
	</div>
	
	<br>
	<p>
	
	<b style="font-size: 2rem;">SII Dental Clinic©</b>
	<p style="vertical-align: middle;">Version: 1.0.0  <a href="about.php" class="fa fa-refresh"></a> </p>
	<p>Develop by: SII™ - ZS31 (Sem 2 S/Y:2017-2018)</p>
	<p>Address: Ateneo Ave, Naga, Camarines Sur</p>
	<p>Contact No: +63XXX-XXX-XXXX</p>


	</p>
	
	
	  <br><br>
	  <!-- End About Us-->
	  
	  	<?php include('footer.php') ?>