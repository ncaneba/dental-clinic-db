<?php

$conn = oci_connect('dental', 'dental', 'localhost/XE');
if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

$sql = 'SELECT * from dentist';
$stid = oci_parse($conn, $sql);
oci_execute($stid);
$numrows = oci_fetch_all($stid, $res);
echo $numrows;


oci_free_statement($stid);
oci_close($conn);

?>
