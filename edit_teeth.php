<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Edit Treatment</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
		<!--update operation-->
		<?php
		if(isset($_POST['edit'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}
  
			$teethcode = isset($_GET['teethcode']) ? $_GET['teethcode'] : '';
  
			$sql = "SELECT *
					FROM teeth
					WHERE teeth_code = :tc";
  
			$stid = oci_parse($conn, $sql);
  
			oci_bind_by_name($stid, ':tc', $teethcode);
  
			oci_execute($stid);
  
			oci_fetch($stid);
  
			$oldteethcode = oci_result($stid, 'TEETH_CODE');
			$oldteethname = oci_result($stid, 'TEETH_NAME');
  
			$teethcode = !empty($_POST['teethcode']) ? $_POST['teethcode'] : $oldteethcode;
			$teethname = !empty($_POST['teethname']) ? $_POST['teethname'] : $oldteethname;

			$sql = "UPDATE teeth
					SET teeth_name = :tn
					WHERE teeth_code = :tc";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tc', $teethcode);
			oci_bind_by_name($stid, ':tn', $teethname);

			oci_execute($stid);
		    header("Location: table_teeth.php");

			oci_close($conn);
		}
		?>
		<!--end update operation-->
		<!--update form-->
		<?php
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$teethcode = $_GET['teethcode'];

			$sql = "SELECT *
					FROM teeth
					WHERE teeth_code = :tc";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':tc', $teethcode);

			oci_execute($stid);
  
			oci_fetch($stid);

			$teethname = oci_result($stid, 'TEETH_NAME');
  
			echo '<div class="container">
<form class="form-inline" action="" method="post" id="needs-validation" novalidate>
  <div class="form-group mx-sm-3 mb-2">
    <label class="sr-only">Teeth Name</label>
    <input type="text" name="teethname" class="form-control" value="'.$teethname.'" placeholder="Teeth Name">
  </div>
  <div class="btn-group" role="group">
  <button class="btn btn-primary mb-2" type="submit" name="edit"><i class ="fa fa-plus" aria-hidden="true"></i></button>
  <a class="btn btn-default mb-2" type="submit"  href="table_teeth.php" ><i class ="fa fa-arrow-left" aria-hidden="true"></i></a>
  </div>
</form>

				  </div>
				  </div>';

			oci_close($conn);
		?>
		<!--end update form-->

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('needs-validation');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
})();
</script>

<script type="text/javascript"> 
// Only Number Input    
function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if ( (charCode > 31 && charCode < 48) || charCode > 57) {
            return false;
        }
        return true;
    }
</script>
	
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>