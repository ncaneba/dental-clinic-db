<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Add Dental chart</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
		<!--Insert operation-->
		<?php
		if(isset($_POST['add'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}
  
			$patientid = $_GET['patientid'];
			
			$sql = "SELECT *
					FROM patient
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);

			$sql = "INSERT INTO dental_chart (chart_id, dates, patient_id)
					VALUES (seq_chart.NEXTVAL, SYSDATE, :pi)";
			
			$stid = oci_parse($conn, $sql);
			
			oci_bind_by_name($stid, ':pi', $patientid);
			
			oci_execute($stid);
			    header("Location: action_chart.php");
		
			oci_close($conn);
		}
		?>
		<!--end Insert operation-->
		<!--update form-->
		<?php
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$patientid = isset($_GET['patientid']) ? $_GET['patientid'] : '';

			$sql = "SELECT *
					FROM patient
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);
  
			oci_fetch($stid);
			
			$pfirstname = oci_result($stid, 'PFIRST_NAME');
			$pmiddleinitial = oci_result($stid, 'PMIDDLE_INITIAL');
			$plastname = oci_result($stid, 'PLAST_NAME');
			$address = oci_result($stid, 'ADDRESS');
			$age = oci_result($stid, 'AGE');
			$pcontactno = oci_result($stid, 'CONTACT_NUM');
  
			echo '<div class="container">
<form class="container" action="" method="post" id="needs-validation" novalidate>
  <div class="row">
    <div class="col-md-5 mb-3">
      <label for="validationCustom01">First name</label>
      <input type="text" name="pfirstname" class="form-control" id="validationCustom01"  value="'.$pfirstname.'" style="text-transform: capitalize" placeholder="First name" disabled >
	  <div class="invalid-feedback">
        This is required.
      </div>
    </div>
	<div class="col-md-2 mb-3">
      <label for="validationCustom01">Middle Initial</label>
      <input type="text" name="pmiddleinitial" class="form-control" id="validationCustom01"  value="'.$pmiddleinitial.'" style="text-transform: capitalize" placeholder="MI" maxlength="2" disabled >
	  <div class="invalid-feedback">
        This is required.
      </div>
    </div>
    <div class="col-md-5 mb-3">
      <label for="validationCustom02">Last name</label>
      <input type="text" name="plastname" class="form-control" id="validationCustom02"  value="'.$plastname.'" style="text-transform: capitalize" placeholder="Last name" disabled >
	  <div class="invalid-feedback">
        This is required.
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-10 mb-3">
      <label for="validationCustom03">Address</label>
      <input type="text" name="address" class="form-control" id="validationCustom03"  value="'.$address.'" style="text-transform: capitalize" placeholder="123 Sample St., Sample City" disabled >
      <div class="invalid-feedback">
        This is required
      </div>
    </div>
    <div class="col-md-2 mb-3">
      <label for="validationCustom03">Age</label>
      <input type="text" name="age" class="form-control" id="validationCustom05"  value="'.$age.'" maxlength="3" onkeypress="return isNumber(event)" placeholder="Age" disabled >
      <div class="invalid-feedback">
        This is required
      </div>
    </div>
    </div>
    <div class="col-md-3 mb-3">
      <label for="validationCustom05">Contact No.</label>
      <input type="text" name="pcontactno" class="form-control" id="validationCustom05"  value="'.$pcontactno.'" maxlength="11" onkeypress="return isNumber(event)" placeholder="09XXXXXXXXX" disabled >
      <div class="invalid-feedback">
        This is required
      </div>
    </div>
	<div class="col-md-3 mb-3">
      <label for="validationCustom05">Dental Chart no.</label>
      <input type="integer" name="chart" class="form-control" id="validationCustom05" maxlength="11" onkeypress="return isNumber(event)" placeholder="Chart No." required>
      <div class="invalid-feedback">
      </div>
    </div>
  </div>
  </div>
  <br>

    <!--<input type="submit" class="btn btn-primary" name="add" value="Add patient"> -->
  <div class="btn-group" role="group">
  <button class="btn btn-primary mb-2" type="submit" name="add"><i class ="fa fa-plus" aria-hidden="true"></i></button>
  <a class="btn btn-default mb-2" type="submit"  href="action_chart.php" ><i class ="fa fa-arrow-left" aria-hidden="true"></i></a>
  </div>
</form>
				  </div>';

			oci_close($conn);
		?>
		<!--end update form-->

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('needs-validation');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
})();
</script>

<script type="text/javascript"> 
// Only Number Input    
function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if ( (charCode > 31 && charCode < 48) || charCode > 57) {
            return false;
        }
        return true;
    }
</script>
	
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>