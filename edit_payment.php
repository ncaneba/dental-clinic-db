<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Edit Payment</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
		<!--update operation-->
		<?php
		if(isset($_POST['edit'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}
  
			$patientid = isset($_GET['paymentid']) ? $_GET['paymentid'] : '';
  
			$sql = "SELECT *
					FROM payment
					WHERE payment_id = :pyid";
  
			$stid = oci_parse($conn, $sql);
  
			oci_bind_by_name($stid, ':pyid', $paymentid);
  
			oci_execute($stid);
  
			oci_fetch($stid);
			
  
			$oldpaymenttype = oci_result($stid, 'TYPE_OF_PAYMENT');
			$oldamountpaid = oci_result($stid, 'AMOUNT_PAID');
			$olddatepaid = oci_result($stid, 'DATE_PAID');
  
			$paymenttype = !empty($_POST['paymenttype']) ? $_POST['paymenttype'] : $oldpaymenttype;
			$amountpaid = !empty($_POST['amountpaid']) ? $_POST['amountpaid'] : $oldamountpaid;
			$datepaid = !empty($_POST['datepaid']) ? $_POST['datepaid'] : $olddatepaid;
			$sql = "UPDATE payment
					SET type_of_payment = :tp, amount_paid = :ap, date_paid = TO_DATE(:dp,'YYYY-MM-DD')
					WHERE patient_id = :pi AND type_of_payment = :tp";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);
			oci_bind_by_name($stid, ':tp', $paymenttype);
			oci_bind_by_name($stid, ':ap', $amountpaid);
			oci_bind_by_name($stid, ':dp', $datepaid);;

			oci_execute($stid);
		    header("Location: action_patient.php");

			oci_close($conn);
		}
		?>
		<!--end update operation-->
		<!--update form-->
				<?php
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$paymentid = $_GET['paymentid'];

			$sql = "SELECT *
					FROM payment
					WHERE payment_id = :pyid";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pyid', $paymentid);

			
			oci_execute($stid);
  
			oci_fetch($stid);


			
			$paymenttype = oci_result($stid, 'TYPE_OF_PAYMENT');
			$amountpaid = oci_result($stid, 'AMOUNT_PAID');
			$datepaid = oci_result($stid, 'DATE_PAID');
			
  
			echo '<div class="container">
<form class="container" action="#" method="post" id="needs-validation" novalidate>
  <div class="form-group">
  </div><br> 
 <div class="row">
  <div class="form-group">

    <label for="validationCustom05">Type of Payment</label>
    <select class="form-control" name="paymenttype" value='.$paymenttype.' id="validationCustom05">
      <option>Partial</option>
      <option>Whole</option>
      <option>Loan</option>
    </select>

  </div>
    <div class="col-md-3 mb-3">
      <label for="validationCustom05">Amount Paid</label>
      <input type="text" name="amountpaid" value='.$amountpaid.' class="form-control" id="validationCustom05" onkeypress="return isNumber(event)" placeholder="0.00" required>
      <div class="invalid-feedback">
        This is required
      </div>
    </div>
	 <div class="col-md-3 mb-3">
      <label for="validationCustom04">Birthday</label>
      <input type="date" name="datepaid" class="form-control" value = '.$datepaid.'  id="validationCustom04" placeholder="MM-DD-YYYY" required>
      <div class="invalid-feedback">
        This is required
      </div>
    </div>
  </div>
  </div>
  <br>

	<button class="btn btn-primary" type="submit" name="edit"><i class="fa fa-pencil aria-hidden="true"></i></button>
</form>
</div>';


			oci_close($conn);
		?>
		<!--end update form-->

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('needs-validation');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
})();
</script>

<script type="text/javascript"> 
// Only Number Input    
function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if ( (charCode > 31 && charCode < 48) || charCode > 57) {
            return false;
        }
        return true;
    }
</script>
	
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>