<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Dental Records</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
	      <!-- Search Tab
	       <form class="form-inline my-2 my-lg-0 mr-lg-2" action="search_records.php" method="get">
            <div class="input-group">
              <input class="form-control" name="treatmentid" placeholder="Search..." type="text">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-primary">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
			<!--
			<li class = "btn btn-space"><a href="insert_treatment.php" class="btn btn-primary" ><i class ="fa fa-plus" aria-hidden="true"></i></a></li>
			<li class = "btn btn-space"><a href="table_treatment.php"><i  class="fa fa-refresh" aria-hidden="true"></i></a></li>

          </form>
		  <br>	-->
<?php
$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$patientid = isset($_GET['patientid']) ? $_GET['patientid'] : '';
			$chart = isset($_GET['chartid']) ? $_GET['chartid'] : '';

			$sql = "SELECT *
					FROM patient
					WHERE patient_id = :pi";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':pi', $patientid);

			oci_execute($stid);
  
			oci_fetch($stid);
			
			$pfirstname = oci_result($stid, 'PFIRST_NAME');
			$plastname = oci_result($stid, 'PLAST_NAME');
			
			echo'
<div>
<h1>
<label>Name :'.$plastname.',</label>
<label>'.$pfirstname.'</label>
<br>
</h1>
<label>Back: </label><li class = "btn btn-space"><a href="table_chart.php?patientid='.$patientid.'" class="btn btn-primary" ><i class ="fa fa-arrow-left" aria-hidden="true"></i></a></li>
</div>';

?>		
		
<?php

// Connects to the XE service (i.e. database) on the "localhost" machine
$conn = oci_connect('dental', 'dental', 'localhost/XE');
if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

$patientid = $_GET['patientid'];
$chartid = $_GET['chartid'];

$stid = oci_parse($conn, 'SELECT * FROM teeth_record WHERE patient_id = :pi AND chart_id = :ci ORDER BY teeth_code	 ASC');

oci_bind_by_name($stid, ':pi', $patientid);
oci_bind_by_name($stid, ':ci', $chartid);

oci_execute($stid);

echo '<table class="table table-bordered" cellspacing="0">
<thread>
				  <tr class="active">
					  <th>Record No.</th>
					  <th>Status</th>
					  <th>Teeth Code</th>
					  <th>Chart ID</th>
					  <th>Patient ID</th>
					  <th>Actions</th>
				  </tr></thread>';
while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
    echo "<tr>\n";
    foreach ($row as $item) {
        echo "    <td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
    }
				echo '<td><div class="btn-group" role="group"><a href="edit_records.php?teethid='.htmlentities($row['TEETH_CODE']).'&&recordno='.htmlentities($row['RECORD_NO']).'&&patientid='.htmlentities($row['PATIENT_ID']).'" class="btn btn-primary"><i class ="fa fa-pencil" aria-hidden="true"></i></a></div></td>';
    echo "</tr>\n";
}
echo "</table>\n";

?>

				</div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>