<?php
    // Start the session
    ob_start();
    session_start();

    // Check to see if actually logged in. If not, redirect to login page
    if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
        header("Location: login.php");
    }
?>

<?php include('header.php') ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Add Dentist</li>
      </ol>
      <div class="row">
        <div class="col-12">
		
		<!--add operation-->
		<?php
		if(isset($_POST['add'])) {
			$conn = oci_connect('dental', 'dental', 'localhost/XE');

			if (!$conn) {
				$e = oci_error();
				trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
			}

			$firstname = isset($_POST['firstname']) ? $_POST['firstname'] : '';
			$middleinitial = isset($_POST['middleinitial']) ? $_POST['middleinitial'] : '';
			$lastname = isset($_POST['lastname']) ? $_POST['lastname'] : '';
			$contactno = isset($_POST['contactno']) ? $_POST['contactno'] : '';

			$sql = "INSERT INTO dentist (doctor_id, fname, minitial, lname, contact_number)
					VALUES (seq_dentist.NEXTVAL, :fn, :mi, :ln, :cn)";

			$stid = oci_parse($conn, $sql);

			oci_bind_by_name($stid, ':fn', $firstname);
			oci_bind_by_name($stid, ':mi', $middleinitial);
			oci_bind_by_name($stid, ':ln', $lastname);
			oci_bind_by_name($stid, ':cn', $contactno);

			oci_execute($stid);
			    header("Location: table_dentist.php");
		
			oci_close($conn);
		}
		?>
		<!--end add operation-->
		<!--add form-->
		<div class="container">
<form class="container" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post" id="needs-validation" novalidate>
  <div class="row">
    <div class="col-md-5 mb-6">
      <label for="validationCustom01">First name</label>
      <input type="text" name="firstname" class="form-control" id="validationCustom01" placeholder="First name" required>
	  <div class="invalid-feedback">
      </div>
    </div>
	<div class="col-md-5 mb-6">
      <label for="validationCustom01">Middle Initial</label>
      <input type="text" name="middleinitial" class="form-control" id="validationCustom01" placeholder="MI" maxlength="2" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-5 mb-6">
      <label for="validationCustom02">Last name</label>
      <input type="text" name="lastname" class="form-control" id="validationCustom02" placeholder="Last name" required>
	  <div class="invalid-feedback">
      </div>
    </div>
    <div class="col-md-5 mb-6">
      <label for="validationCustom05">Contact No.</label>
      <input type="text" name="contactno" class="form-control" id="validationCustom05" maxlength="11" onkeypress="return isNumber(event)" placeholder="09XXXXXXXXX" required>
      <div class="invalid-feedback">
      </div>
    </div>
  </div>
  </div>
    <!--<input type="submit" class="btn btn-primary" name="add" value="Add Dentist"> -->
	<br>
  <div class="btn-group" role="group" class="col-md-5">
  <button class="btn btn-primary mb-2" type="submit" name="add"><i class ="fa fa-plus" aria-hidden="true"></i></button>
  <a class="btn btn-default mb-2" type="submit"  href="table_dentist.php" ><i class ="fa fa-arrow-left" aria-hidden="true"></i></a>
  </div>
</form>
		<!--end add form-->

				</div>
				</div>
        </div>
      </div>
    </div>

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('needs-validation');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
})();
</script>

<script type="text/javascript"> 
// Only Number Input    
function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if ( (charCode > 31 && charCode < 48) || charCode > 57) {
            return false;
        }
        return true;
    }
</script>
	
    <!-- /.container-fluid-->
	<?php include('footer.php') ?>